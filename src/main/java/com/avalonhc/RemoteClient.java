package com.avalonhc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;





import org.drools.command.BatchExecutionCommand;
import org.drools.command.CommandFactory;
import org.drools.core.command.GetVariableCommand;
import org.kie.api.command.Command;
import org.kie.api.task.model.Task;
import org.kie.remote.client.api.RemoteWebserviceClientBuilder;
import org.kie.remote.client.jaxb.JaxbCommandsRequest;
import org.kie.remote.client.jaxb.JaxbCommandsResponse;
import org.kie.remote.jaxb.gen.GetTaskCommand; 
import org.kie.remote.jaxb.gen.GetTaskContentCommand;
import org.kie.remote.jaxb.gen.GetTasksByProcessInstanceIdCommand;
import org.kie.remote.jaxb.gen.JaxbStringObjectPairArray;
import org.kie.remote.jaxb.gen.StartProcessCommand;
import org.kie.remote.jaxb.gen.util.JaxbStringObjectPair;
import org.kie.remote.services.ws.command.generated.CommandWebService;
import org.kie.remote.services.ws.command.generated.CommandWebServiceException;
import org.kie.services.client.api.RemoteRuntimeEngineFactory;
import org.kie.services.client.serialization.jaxb.impl.JaxbCommandResponse;
import org.kie.services.client.serialization.jaxb.impl.JaxbLongListResponse;
import org.kie.services.client.serialization.jaxb.impl.process.JaxbProcessInstanceResponse;
import org.slf4j.Logger;

import com.avalonhc.remote.ClaimRequest;
import com.avalonhc.remote.EvaluateLabClaim;
import com.avalonhc.remote.EvaluateLabClaimResponse;
import com.avalonhc.remote.RequestHeaderData;
import com.avalonhc.remote.RequestLineData;
import com.avalonhc.remote.ResponseHeaderData;
import com.avalonhc.remote.ResponseLineData;
import com.avalonhc.remote.SecondaryCodes;



public class RemoteClient {

	static String jbpmUrl = "http://localhost:8080/jbpm-console/";
	static String user="krisv";
	static String password = "krisv";
	static String deploymentId = "org.jboss.as.facts:Claim:2.1.8-beta";
	static String processId = "defaultPackage.GlobalFlow";
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
  RemoteClient remoteClient = new RemoteClient();	
		
		URL applicationURL = new URL (jbpmUrl);	
	EvaluateLabClaim eval = new EvaluateLabClaim();
	RequestHeaderData header = new RequestHeaderData();
	header.setIdCardNumber("ZCL06440277");
	header.setHealthPlanId("401");
	header.setHealthPlanIdType("EI");
	header.setHealthPlanGroupId("031234547");
	header.setClaimNumber("10202");
	header.setHealthPlanMemberId("1123S");
	header.setPatientLastName("SQUAD");
	header.setPatientFirstName("SID");
	header.setIcdDiagVersQualifier("10");
	DatatypeFactory dtf = DatatypeFactory.newInstance();
	XMLGregorianCalendar dob = dtf.newXMLGregorianCalendarDate(1972, 05, 20,-6);
	XMLGregorianCalendar fds = dtf.newXMLGregorianCalendarDate(2015, 01, 06, -6);
	XMLGregorianCalendar tds = dtf.newXMLGregorianCalendarDate(2015, 01, 07, -6);
	header.setPatientDateOfBirth(dob);
	header.setPatientGenderCode("M");
	header.setPrimaryDiagnosisCode("A");
	ArrayList<String> diagnosisCodes = new ArrayList();
	diagnosisCodes.add("M810");
	diagnosisCodes.add("shshs");
	header.getDiagnosisCodes().addAll(diagnosisCodes);
	header.setBillingProviderTaxId("123456777");
	header.setBillingProviderNpi("123456777");
	header.setNumberOfLines(1);
    header.setFromDateOfService(fds);
    header.setToDateOfService(tds);
    header.setLineOfBusiness("4");
	
    RequestLineData rds = new RequestLineData();
	rds.setLineNumber(1);
	rds.setFromDateOfService(fds);
	rds.setToDateOfService(tds);
	rds.setPlaceOfService("81");
	rds.setProcedureCode("86304");
	//rds.getProcedureCodeModifiers().add("00");
	rds.setUnits(12);
	rds.getDiagnosisCodePointers().add("1");
	
	rds.setRenderingProviderNpi("1234567");
	rds.setInNetworkIndicator("Y");
	rds.setPaidDeniedIndicator("P");
	eval.setRequestHeader(header);
    eval.getRequestLine().add(rds);	
    JaxbCommandsResponse res = startSimpleProcess(applicationURL,user,password,deploymentId, processId,eval);	

	}
	
	 private static CommandWebService createWebserviceClient(URL applicationUrl,
		        String user, String password, String deploymentId) {
		        CommandWebService client =
		        RemoteRuntimeEngineFactory.newCommandWebServiceClientBuilder()
		        .addServerUrl(applicationUrl)
		        .addUserName(user)
		        .addPassword(password)
		        .addDeploymentId(deploymentId)
		        .addExtraJaxbClasses(ClaimRequest.class, EvaluateLabClaimResponse.class,SecondaryCodes.class, ResponseHeaderData.class, ResponseLineData.class, EvaluateLabClaim.class)
		        .buildBasicAuthClient();
		        
		        return client;
		        }	
	 
	 private static <T> T doWebserviceSingleCommandRequest(CommandWebService service,
		        Command<?> cmd, Class<T> respClass, String deploymentId)
		        throws CommandWebServiceException {
		        // Get a response from the WebService
		        JaxbCommandsRequest req = new JaxbCommandsRequest(deploymentId, cmd);
		    //    GetVariableCommand getVariable = new GetVariableCommand(identifier)     
		        JaxbCommandsResponse response = null;
		        service.execute(req);
		        // List resp = service.execute(req).getResponses();
		        // check response
		        //JaxbCommandResponse<?> cmdResp = response.getResponses().get(0);
		        return null;
		        //return (T) cmdResp;
		        }
	 
	 public  static JaxbCommandsResponse startSimpleProcess(URL applicationUrl,
		        String user, String password, String deploymentId, String processId, EvaluateLabClaim evaluateLabClaim)
		        throws Exception {
		        
		        CommandWebService commandWebService
		        = createWebserviceClient(applicationUrl, user, password, deploymentId);
		        Map params = getMapFromEvaluateClaimRequest(evaluateLabClaim);
		        // Create start process command
		        
		        StartProcessCommand spc = new StartProcessCommand();
		       
		        spc.setProcessId(processId);
		        JaxbStringObjectPairArray map = new JaxbStringObjectPairArray();
		        JaxbStringObjectPair keyValue1 = new JaxbStringObjectPair();
		        JaxbStringObjectPair keyValue2 = new JaxbStringObjectPair();
		        JaxbStringObjectPair keyValue3 = new JaxbStringObjectPair();
		        JaxbStringObjectPair keyValue4 = new JaxbStringObjectPair();
		        keyValue1.setKey("claimReq");
		        keyValue1.setValue(params.get("claimReq"));
		        map.getItems().add(keyValue1);
		       
		        keyValue2.setKey("evaluateLabClaimResponse");
		        EvaluateLabClaimResponse resp= (EvaluateLabClaimResponse) params.get("evaluateLabClaimResponse");
		        keyValue2.setValue(resp);
		        map.getItems().add(keyValue2);
		        keyValue3.setKey("secondaryCodes");
		        keyValue3.setValue(params.get("secondaryCodes"));
		      map.getItems().add(keyValue3);
		        
		        keyValue4.setKey("responseLineData");
		        keyValue4.setValue(params.get("responseLineData"));
		      map.getItems().add(keyValue4);
		        
		        
		        /**
		         * 
		         * params.put("claimReq", claimRequest);
   	        	 params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
   	        	 params.put("secondaryCodes", secondaryCodes);
   	        	 params.put("responseLineData", responseLineData);
		         */
		        spc.setParameter(map);
		        
		      //  spc.setOutIdentifier("evaluateLabClaimResponse");
		        
		        // Do webService request
		        JaxbProcessInstanceResponse jpir
		        = doWebserviceSingleCommandRequest(commandWebService, spc,
		        JaxbProcessInstanceResponse.class, deploymentId);
		        long procInstId = ((JaxbProcessInstanceResponse) jpir).getId();
		        
		        // Create command
		        GetTasksByProcessInstanceIdCommand gtbic = new GetTasksByProcessInstanceIdCommand();
		        gtbic.setProcessInstanceId(procInstId);
		        
		        // Do webservice request
		        JaxbLongListResponse jllr
		        = doWebserviceSingleCommandRequest(commandWebService, gtbic,
		        JaxbLongListResponse.class, deploymentId );
		        List<Long> taskIds = jllr.getResult();
		        long taskId = taskIds.get(0);
		        
		        // Commands for task and task content
		        GetTaskCommand gtc = new GetTaskCommand();
		        gtc.setTaskId(taskId);
		        GetTaskContentCommand gtcc = new GetTaskContentCommand();
		        gtcc.setTaskId(taskId);
		        
		        // Do webservice request (with both commands)
		        JaxbCommandsRequest req = new JaxbCommandsRequest(deploymentId, gtc);
		        req.getCommands().add(gtcc); 
		        JaxbCommandsResponse response = commandWebService.execute(req);
		        
		        // Get task and content response
		        Task task = (Task) response.getResponses().get(0).getResult();
		        Map<String, Object> contentMap
		        = (Map<String, Object>) response.getResponses().get(1).getResult();
		        
		        return response;
		        }
		        
	
  static Map  getMapFromEvaluateClaimRequest(EvaluateLabClaim evaluateLabClaim) throws ParseException{
	  Map<String, Object> params = new HashMap<String, Object>();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        ClaimRequest claimRequest =new ClaimRequest();
        System.out.println("healthpland:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
        if(null!= evaluateLabClaim.getRequestHeader().getHealthPlanId() && !"".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId()) && !"?".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId())){
    		claimRequest.setHealthPlanId(evaluateLabClaim.getRequestHeader().getHealthPlanId().trim());
    		System.out.println("Health plan:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
    		}
    	
   	        	
        if(null!=evaluateLabClaim.getRequestLine().get(0).getProcedureCode() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())){
	 		   claimRequest.setProcedureCode(evaluateLabClaim.getRequestLine().get(0).getProcedureCode().trim());
	 		   System.out.println("procedure code:::"+ evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
	 		} 
   	         
        if(null!=evaluateLabClaim.getRequestLine().get(0).getFromDateOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService())){
	     
        	claimRequest.setFromDateOfService(sdf.parse(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService().toString().trim()));
       	 //claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
	        	 System.out.println("From Date::"+evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
	         }
   	      
	        	 System.out.println("DOB::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth() );
	        	 System.out.println("primaryDiagnosis:::"+evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());
	        	 
	        	 if(null!=evaluateLabClaim.getRequestLine().get(0).getPlaceOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService())){
	 	        	   claimRequest.setPlaceOfService(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService().trim());
	    	        	 System.out.println("PlaceOfService::::"+evaluateLabClaim.getRequestLine().get(0).getPlaceOfService());
	 	        	  }
   	        	 //setting PrimaryDiagnosisCode and DiagnosisCode in List for comparing with single column value in decision table
	        	 claimRequest.setError("NO");
	        	 List<String> diagnosisCodesList =new ArrayList<String>();
	        	 int DiagnosisCodePointersCount=0;
	        	
	        	 if(null== evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() || "".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()) || "?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
	        		 if (null != evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0) && !"".equals(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0))){ 
	   	        	  DiagnosisCodePointersCount =Integer.parseInt(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0));
	        		 claimRequest.setDiagnosisCodePointers(DiagnosisCodePointersCount);
	        	 }
	        	 }
	        	 int count = evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size(); 
	        	 
   	        	 System.out.println("Step 1 diagnosisCodesList ---->");
   	        	 
   	        	
   	        		if(null!= evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()) && !"?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
   	      	        	 diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
   	      	        	 claimRequest.setPrimaryDiagnosisCode(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
   	      	        	}
   	        		System.out.println("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	  	        		System.out.println("in for loop the value of count1"+count);
   	  	        	if(null== evaluateLabClaim.getRequestHeader().getDiagnosisCodes() || "".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes()) || "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
   	  	        		System.out.println("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
   	  	        		System.out.println("in for loop the value of count"+count);
   	  	        		for (int i = 0; i < count; i++) {
   	  	        			System.out.println("in for loop the value of i"+i);
   	  	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
   	  	        			System.out.println("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
   	  	      		}
   	  	       
   	  	        	 
   	  	        	}
   	        		 System.out.println("Step 1 diagnosisCodesList ---->");
   	      	        	if(null!= evaluateLabClaim.getRequestHeader().getDiagnosisCodes() && !"".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())&& ! "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
   	      	        	// diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
   	      	        		for(int  i=0;i<evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size();i++){
   	      	        			System.out.println("diagnosisCodesList ---->"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
   	      	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
   	      	        		}
   	      	        		
   	      	        	}
   	      	        	System.out.println("diagnosisCodesList ---->"+diagnosisCodesList); 
   	      	        	claimRequest.getDiagnosisCodesList().addAll(diagnosisCodesList);
   	     	        	 
   	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientGenderCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode()) ){
   	        	        	 System.out.println("Gender::::"+evaluateLabClaim.getRequestHeader().getPatientGenderCode());
   	        	        	 claimRequest.setPatientGenderCode(evaluateLabClaim.getRequestHeader().getPatientGenderCode().trim());
   	        	        	}
   	     	        	
   	     	        	
   	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientDateOfBirth() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()) ){ 
   	     	        		
   	     	        		claimRequest.setPatientDOB((evaluateLabClaim.getRequestHeader().getPatientDateOfBirth().toString().trim()));
   	     	        		 System.out.println("Patient DOB::::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth());
   	     	        	}
   	     	        	
   	     	        	if((evaluateLabClaim.getRequestLine().get(0).getUnits())!= 0 && !"".equals(evaluateLabClaim.getRequestLine().get(0).getUnits()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getUnits())){
   	 	 	        	   claimRequest.setUnits(evaluateLabClaim.getRequestLine().get(0).getUnits());
   	 	    	        	 System.out.println("Units::::"+evaluateLabClaim.getRequestLine().get(0).getUnits());
   	 	 	        	  }
   	     	        	
   	     	        	else
   	     	        	{
   	     	        		claimRequest.setPatientDOB(null);
   	     	        	}
   	     	        	
   	     	        	 	
                
    	        	//Sample Response creation for global flow
   	     	        	UUID uuid = UUID.randomUUID();
   	     			    String randomUUIDString = uuid.toString();
   	     	        	
    	        	ResponseHeaderData responseHeaderData = new ResponseHeaderData();
    	        	
    	        	System.out.println("randomUUIDString : : : "+randomUUIDString);
    	        	responseHeaderData.setCeTransactionId(randomUUIDString);
    	        
    	        	responseHeaderData.setClaimNumber(evaluateLabClaim.getRequestHeader().getClaimNumber());
   	        	System.out.println("ClaimNumber::::"+evaluateLabClaim.getRequestHeader().getClaimNumber());	 
	            	EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
    	        	SecondaryCodes secondaryCodes = new SecondaryCodes();
    	        	ResponseLineData responseLineData =new ResponseLineData();
    	        	
    	        	
    	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
    	        	evaluateLabClaimResponse.getResponseLine().add(responseLineData);
    	      //  	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
    	        	
   	        	 
   	        	 params.put("claimReq", claimRequest);
   	        	 params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
   	        	 params.put("secondaryCodes", secondaryCodes);
   	        	 params.put("responseLineData", responseLineData);
	  
   	        	 	return params;
	  
	  
	  
	  
  }


}




