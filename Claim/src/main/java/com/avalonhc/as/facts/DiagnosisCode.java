package com.avalonhc.as.facts;

public class DiagnosisCode {

	private String diagnosisCode;

	public String getDiagnosisCode() {
		return diagnosisCode;
	}

	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}
}
