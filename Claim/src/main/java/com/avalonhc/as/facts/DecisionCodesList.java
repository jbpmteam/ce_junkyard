package com.avalonhc.as.facts;

public class DecisionCodesList {

	private String policyTag;
	private String policyNecessityCriteria;
	private String decisionCode;

	public String getDecisionCode() {
		return decisionCode;
	}

	public void setDecisionCode(String decisionCode) {
		this.decisionCode = decisionCode;
	}
	
	public String getPolicyTag() {
		return policyTag;
	}

	public void setPolicyTag(String policyTag) {
		this.policyTag = policyTag;
	}
	
	public String getPolicyNecessityCriteria() {
		return policyNecessityCriteria;
	}

	public void setPolicyNecessityCriteria(String policyNecessityCriteria) {
		this.policyNecessityCriteria = policyNecessityCriteria;
	}
}
